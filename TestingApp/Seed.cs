﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestingApp.Entities;

namespace TestingApp
{
    public static class Seed
    {
        //Have the issue: rows are saved in database in random order
        public static async Task<IApplicationBuilder> UseSeedAsync(this IApplicationBuilder app)
        {
            using (var scope = app.ApplicationServices.CreateScope())
            {
                var context =
                    (AppContext)scope.ServiceProvider.GetService(typeof(AppContext));

                var user1 = new User { Email = "email1@gmail.com", Password = "password1" };
                var user2 = new User { Email = "email2@gmail.com", Password = "password2" };
                var user3 = new User { Email = "email3@gmail.com", Password = "password3" };

                //This need to add entities in properly order
                context.Users.Add(user1);
                context.Users.Add(user2);
                context.Users.Add(user3);               

                var test1 = new Test
                {
                    Title = "First test",
                    Description = "First test description",
                    Users = new List<User>(){
                            user1, user3
                    },
                    Questions = new List<Question>(){
                        new Question { Text = "First test first question", Answers = new List<Answer>() {
                            new Answer { Text="First test first question Right answer", IsRight = true },
                            new Answer { Text="First test first question False answer", IsRight = false },
                            new Answer { Text="First test first question False answer", IsRight = false }
                        } },
                        new Question { Text = "First test second question", Answers = new List<Answer>() {
                            new Answer { Text="First test second question False answer", IsRight = false },
                            new Answer { Text="First test second question Right answer", IsRight = true },
                            new Answer { Text="First test second question False answer", IsRight = false }
                        } },
                    }
                };

                var test2 = new Test
                {
                    Title = "Second test",
                    Description = "Second test description",
                    Users = new List<User>(){
                            user1, user2
                    },
                    Questions = new List<Question>(){
                        new Question { Text = "Second test first question", Answers = new List<Answer>() {
                            new Answer { Text="Second test first question Right answer", IsRight = true },
                            new Answer { Text="Second test first question False answer", IsRight = false },
                            new Answer { Text="Second test first question False answer", IsRight = false }
                        } },
                        new Question { Text = "Second test second question", Answers = new List<Answer>() {
                            new Answer { Text="Second test second question False answer", IsRight = false },
                            new Answer { Text="Second test second question Right answer", IsRight = true },
                            new Answer { Text="Second test second question False answer", IsRight = false }
                        } },
                    }
                };

                var test3 = new Test
                {
                    Title = "Third test",
                    Description = "Third test description",
                    Users = new List<User>(){
                            user2, user3
                    },
                    Questions = new List<Question>(){
                        new Question { Text = "Third test first question", Answers = new List<Answer>() {
                            new Answer { Text="Third test first question Right answer", IsRight = true },
                            new Answer { Text="Third test first question False answer", IsRight = false },
                            new Answer { Text="Third test first question False answer", IsRight = false }
                        } },
                        new Question { Text = "Third test second question", Answers = new List<Answer>() {
                            new Answer { Text="Third test second question False answer", IsRight = false },
                            new Answer { Text="Third test second question Right answer", IsRight = true },
                            new Answer { Text="Third test second question False answer", IsRight = false }
                        } },
                    }
                };

                user1.Tests = new List<Test>() { test1, test2 };
                user2.Tests = new List<Test>() { test2, test3 };
                user3.Tests = new List<Test>() { test1, test3 };

                context.SaveChanges();
                return app;
            }
        }
    }
}
