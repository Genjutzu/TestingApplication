﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using TestingApp.Entities;

namespace TestingApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class TestController : ControllerBase
    {
        private readonly AppContext context;

        public TestController(AppContext context)
        {
            this.context = context;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var test = context.Tests.Where(x => x.Id == id).Include(x => x.Questions)
                    .ThenInclude(x => x.Answers).First();
                
                foreach(var question in test.Questions)
                {
                    foreach(var answer in question.Answers)
                    {
                        answer.IsRight = false;
                    }
                }

                return Ok(test);
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                var user = context.Users.Where(x => x.Id == int.Parse(HttpContext.User.FindFirst("ID").Value)).Include(x => x.Tests).First();
                return Ok(user.Tests);
            }
            catch(Exception e)
            {
                return BadRequest(e);
            }
        }
        
        [HttpGet("result/params")]
        public async Task<IActionResult> GetResult([FromQuery] string[] ids)
        {
            try
            {
                var rightAnswersCount = context.Answers.Where(x => ids.Select(int.Parse).ToList().Contains(x.Id) && x.IsRight == true).Count();
                return Ok(new { rightAnswersCount, allAnswers = ids.Length });
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }
    }
}
