import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Test from '../views/Test.vue'
import TestResult from '../views/TestResult.vue'
import Login from '../views/Login.vue'
import { store } from '@/store/index';

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Home',
        component: Home,
        meta: { requiresAuth: true }
    },
    {
        path: '/test/:id',
        name: 'Test',
        component: Test,
        meta: { requiresAuth: true }
    },
    {
        path: '/result',
        name: 'TestResult',
        component: TestResult,
        meta: { requiresAuth: true }
    },
    {
        path: '/login',
        name: 'Login',
        component: Login
    }
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (!store.getters['account/isAuthenticated']) {
            next({
                path: '/login'
            })
        }
        else {
            next();
        }
    } else {
        next();
    }
});

export default router
