import axios from 'axios';
import store from '@/store/account';

const service = axios.create({
    baseURL: 'https://localhost:44366',
    timeout: 15000
});

service.interceptors.request.use(
    (config) => {
        const token = store.state.token;
        if (token) {
            config.headers.Authorization = "Bearer " + token;
        }
        return config;
    },
    (error) => {
        console.log(error);
        return Promise.reject(error);
    }
);

export default service;
