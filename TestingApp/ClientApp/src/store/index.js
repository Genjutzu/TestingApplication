import Vue from 'vue'
import Vuex from 'vuex'
import account from '@/store/account';
import test from '@/store/test';

Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
    modules: {
        account,
        test
  }
})
