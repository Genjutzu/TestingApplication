import api_service from '@/services/api-service'

const api = api_service;

const state = {
    test: null,
    tests: null,
    result: null
};

const getters = {
    getTest: state => {
        return state.test;
    },
    getTests: state => {
        return state.tests;
    },
    getResult: state => {
        return state.result;
    }
};

const mutations = {
    SET_TEST: (state, data) => {
        state.test = data;
    },
    SET_TESTS: (state, data) => {
        state.tests = data;
    },
    SET_RESULT: (state, data) => {
        state.result = data;
    }
};

const actions = {
    GetTest: async (context, payload) => {
        return api.get("/api/test/" + payload).then((response) => context.commit('SET_TEST', response.data));
    },
    GetTests: async (context) => {
        return api.get("/api/test").then((response) => context.commit('SET_TESTS', response.data));
    },
    GetResult: async (context, payload) => {
        var idsUri = ""
        payload.forEach(element => idsUri += "&ids=" + element);
        idsUri = idsUri.substring(1);
        return api.get("/api/test/result/params?" + idsUri).then((response) => context.commit('SET_RESULT', response.data));
    }
};

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions,
};